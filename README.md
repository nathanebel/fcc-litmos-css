# FCC-Litmos-CSS

Custom CSS file for Full Circle's Litmos platform. For use on Litmos, this needs to be saved in the Themes tab of settings.